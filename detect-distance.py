from dotenv import load_dotenv
load_dotenv(verbose=True)

import os
import sys
import paho.mqtt.client as mqtt
import threading
import jetson.inference
import jetson.utils

import argparse

mqttHostname = os.getenv("MQTT_HOSTNAME")
mqttPort = os.getenv("MQTT_PORT")
mqttUsername = os.getenv("MQTT_USERNAME")
mqttPassword = os.getenv("MQTT_PASSWORD")

# parse the command line
parser = argparse.ArgumentParser(description="Classify a live camera stream using an image recognition DNN.", 
						   formatter_class=argparse.RawTextHelpFormatter, epilog=jetson.inference.imageNet.Usage())

parser.add_argument("--network", type=str, default="googlenet", help="pre-trained model to load (see below for options)")
parser.add_argument("--camera", type=str, default="0", help="index of the MIPI CSI camera to use (e.g. CSI camera 0)\nor for VL42 cameras, the /dev/video device to use.\nby default, MIPI CSI camera 0 will be used.")
parser.add_argument("--width", type=int, default=1280, help="desired width of camera stream (default is 1280 pixels)")
parser.add_argument("--height", type=int, default=720, help="desired height of camera stream (default is 720 pixels)")
parser.add_argument("--overlay", type=str, default="box,labels,conf", help="detection overlay flags (e.g. --overlay=box,labels,conf)\nvalid combinations are:  'box', 'labels', 'conf', 'none'")
parser.add_argument("--threshold", type=float, default=0.5, help="minimum detection threshold to use") 

try:
	opt = parser.parse_known_args()[0]
except:
	print("")
	parser.print_help()
	sys.exit(0)

# load the recognition network
#net = jetson.inference.imageNet(opt.network, sys.argv)
net = jetson.inference.detectNet(opt.network, sys.argv, opt.threshold)

# create the camera and display
font = jetson.utils.cudaFont()
camera = jetson.utils.gstCamera(opt.width, opt.height, opt.camera)
display = jetson.utils.glDisplay()

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("$SYS/#")

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set(mqttUsername, password=mqttPassword)
client.connect(mqttHostname, int(mqttPort), 60)

def captureAndDetectObject():
    while display.IsOpen():
	img, width, height = camera.CaptureRGBA()

	with open('distance.dat', 'r') as file:
         data = file.read().replace('\n', '')
         print(data)
         # detect objects in the image (with overlay)
	 if data != 'No distance yet.':
    		overlayOvv = opt.overlay
	 else:
		overlayOvv = 'none'

    	 detections = net.Detect(img, width, height, overlayOvv)

	 # print the detections
	 print("detected {:d} objects in image".format(len(detections)))
	 if detections> 0:
		ret= client.publish("detection/nos",len(detections))
		print(ret);

	 for detection in detections:
		print(detection)
        	font.OverlayText(img, width, height, data, 5, 50, font.White, font.Gray40)
        	display.RenderOnce(img, width, height)
        	display.SetTitle("{:s} | Network {:.0f} FPS".format(opt.network, net.GetNetworkFPS()))
        	net.PrintProfilerTimes()

if __name__ == "__main__":
 captureAndDetectObject()
