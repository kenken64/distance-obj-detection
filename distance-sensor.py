from dotenv import load_dotenv
load_dotenv(verbose=True)

import os
import sys
import serial
import paho.mqtt.client as mqtt
import threading

import argparse

serialName = os.getenv("SERIAL_NAME")
serialPort = os.getenv("SERIAL_PORT")
mqttHostname = os.getenv("MQTT_HOSTNAME")
mqttPort = os.getenv("MQTT_PORT")
mqttUsername = os.getenv("MQTT_USERNAME")
mqttPassword = os.getenv("MQTT_PASSWORD")

print(serialPort)
ser = serial.Serial(serialName, int(serialPort), timeout=None)


global distanceFolder
distanceFolder = '100mm'
distanceMessage = "No distance yet."

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("$SYS/#")
    client.subscribe("toggle/distance",1)

def on_message(client, userdata, msg):
    global distanceFolder
    global distanceMessage
    print(msg.topic+" "+str(msg.payload))
    if "mm" in str(msg.payload):
     print('assign ....')
     distanceFolder = msg.payload.decode('utf-8')
     distanceMessage = 'No distance yet.'

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set(mqttUsername, password=mqttPassword)
client.connect(mqttHostname, int(mqttPort), 60)


def listenToMqtt():
 while True:
  client.loop()

def readDistanceSensor():
 global distanceMessage
 while True:
    ser.flushInput()
    cc=str(ser.readline())
    print(distanceFolder)
    ccc = cc[0:][:-5]
    print(ccc)
    splitarr = ccc.split('|')
    if len(splitarr) > 2:
        print(splitarr[1])
        if splitarr[1] == 'range valid':
            print(splitarr[0])
            if splitarr[0] != '':
                if int(splitarr[0]) <= 100 and distanceFolder == '100mm': 
                    distanceMessage = 'below 100mm'
                    print(distanceMessage)
                elif int(splitarr[0]) > 100 and int(splitarr[0]) <= 200 and distanceFolder == '200mm':
                    distanceMessage = 'in between 100mm and 200mm'
                    print(distanceMessage)
                elif int(splitarr[0]) > 200 and int(splitarr[0]) <= 300 and distanceFolder == '300mm':
                    distanceMessage = 'in between 200mm and 300mm'
                    print(distanceMessage)
                elif int(splitarr[0]) > 300 and int(splitarr[0]) <= 400 and distanceFolder == '400mm':
                    distanceMessage = 'in between 300mm and 400mm'
                    print(distanceMessage)
                elif int(splitarr[0]) > 400 and distanceFolder == '>400mm':
                    distanceMessage = 'above 400mm'
                    print(distanceMessage)
                else:
                    distanceMessage = 'No distance yet.'
		with open("distance.dat", "w") as text_file:
                  text_file.write(distanceMessage)
        else:
          print('No distance yet.')
          distanceMessage = 'No distance yet.'
          print(distanceMessage)
          with open("distance.dat", "w") as text_file:
            text_file.write(distanceMessage)

if __name__ == "__main__":
 try:
  tasks = [readDistanceSensor, listenToMqtt]
  for task in tasks:
   t = threading.Thread(target=task)
   t.start()
 except KeyboardInterrupt:
  print("distance sensor prog exited!")
