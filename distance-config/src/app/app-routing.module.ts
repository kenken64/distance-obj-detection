import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DistanceConfigComponent } from './components/distance-config/distance-config.component';

const routes: Routes = [
  { path: '', component: DistanceConfigComponent },
  { path: '**', redirectTo: '/', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
