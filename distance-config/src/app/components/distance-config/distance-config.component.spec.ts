import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistanceConfigComponent } from './distance-config.component';

describe('DistanceConfigComponent', () => {
  let component: DistanceConfigComponent;
  let fixture: ComponentFixture<DistanceConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistanceConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistanceConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
