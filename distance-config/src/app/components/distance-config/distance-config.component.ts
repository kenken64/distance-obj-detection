import { Component, OnInit, OnDestroy } from '@angular/core';
import { coerceNumberProperty } from '@angular/cdk/coercion';
import { Subscription } from 'rxjs';
import {
  IMqttMessage,
  MqttService,
} from 'ngx-mqtt';

import {Howl, Howler} from 'howler';

@Component({
  selector: 'app-distance-config',
  templateUrl: './distance-config.component.html',
  styleUrls: ['./distance-config.component.css']
})
export class DistanceConfigComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  autoTicks = false;
  disabled = false;
  invert = false;
  max = 400;
  min = 100;
  showTicks = false;
  step = 100;
  thumbLabel = true;
  value = 0;
  vertical = false;
  private _tickInterval = 1;
  public message: string;
  public noOfDetections: string;
  aboveMax = false;
  aboveMaxChecked = false;
  
  sound = new Howl({
      src: ['../../assets/Siren_Noise.mp3'],
      loop: true
  });

  constructor(private _mqttService: MqttService) { 
    this.subscription = this._mqttService.observe('toggle/distance').subscribe((message: IMqttMessage) => {
      this.message = message.payload.toString();
    });

    this.subscription = this._mqttService.observe('detection/nos').subscribe((message: IMqttMessage) => {
      this.noOfDetections = message.payload.toString();
      if(+this.noOfDetections > 0){
        if(!this.sound.playing()){
          if(this.sound)
            this.sound.stop();
          // Play the sound.
          this.sound.play();
          // Change global volume.
          Howler.volume(0.3);
        }
      }else{
        if(this.sound)
            this.sound.stop();
      }
    });
  }

  ngOnInit() {
    this.sound.stop();
    this.aboveMaxChecked = false;
  }

  get tickInterval(): number | 'auto' {
    return this.showTicks ? (this.autoTicks ? 'auto' : this._tickInterval) : 0;
  }

  set tickInterval(value) {
    this._tickInterval = coerceNumberProperty(value);
    console.log(this._tickInterval);
  }

  onChange(event){
    console.log(event);
    this.sendToggleDistance(event);
  }

  sendToggleDistance(value){
    this._mqttService.publish('toggle/distance', value+ 'mm')
      .subscribe();
  }

  setAboveMax(){
    console.log("Set above 400mm ");
    if(this.aboveMaxChecked == true){
      this.aboveMaxChecked = false;
      this.disabled = false;
      console.log(this.value);
      this.sendToggleDistance(this.value);
    }else{
      this.aboveMaxChecked = true;
      this.disabled = true;
      this.sendToggleDistance(">400");
    }
  }
  
  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
