##  Object detection using Nvidia Jetson

![alt text](jetson1.jpg#fixorientation "hardware")
![alt text](jetson2.jpg#fixorientation "detection")

### Bill of materials
* 1 x [Nvidia Jetson Nano](https://www.sgbotic.com/index.php?dispatch=products.view&product_id=2846)
* 1 x [160 degree CSI Camera](https://www.sgbotic.com/index.php?dispatch=products.view&product_id=2848)
* 1 x [Acrylic Protective Case with Cooling Fan with Camera Stand](https://category.yahboom.net/products/jetson-nano-case)
* 1 x [Jetson Nano Wifi adapter](https://www.sgbotic.com/index.php?dispatch=products.view&product_id=2851)
* 2 x [Jetson Nano Wifi antenna](https://www.sgbotic.com/index.php?dispatch=products.view&product_id=2853)
* 1 x [Arduino Uno](https://www.sgbotic.com/index.php?dispatch=products.view&product_id=2536)
* 1 x [Arduino Uno Prototype board shield](https://sg.cytron.io/p-prototyping-shield?r=1&gclid=CjwKCAiA8qLvBRAbEiwAE_ZzPQ5AnItbpzV_WO1TOmOqA7T1njGMi5Sr3rm0blWa3T0zWcoD1qNghRoCJaQQAvD_BwE)
* 1 x [VL53L1X Time-of-Flight Distance Sensor Carrier (4M)](https://www.pololu.com/product/3415)


### Tech Stack used
* Python (Pyserial, Jetson inteference library)
* Angular
* Arduino ( Proximity sensor firmware)

### How to run the program
The first python program sense distance of the object and the second python program uses the csi camera which is hooked to the jetson nano to detect object using the pre-trained model from dataset provider.
```
$ git clone <this repo>
$ cd distance-obj-detection
$ python distance-sensor.py
$ ./run-distance2.sh
```
## How to run the angular web app
This app is to control the distance of the time flight sensor.
```
$ cd distance-config
$ npm i
$ ng serve
```